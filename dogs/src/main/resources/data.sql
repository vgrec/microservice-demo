INSERT INTO dog (id, name, breed, origin) VALUES (1, 'Mishanea', 'Parodistnii', 'Sabornii Les');
INSERT INTO dog (id, name, breed, origin) VALUES (2, 'Sharik', 'Prisacar', 'Cania');
INSERT INTO dog (id, name, breed, origin) VALUES (3, 'Laica', 'Ciobanesc', 'Cociulia');
INSERT INTO dog (id, name, breed, origin) VALUES (4, 'Haiduc', 'Muntenesc', 'Transilvania');
INSERT INTO dog (id, name, breed, origin) VALUES (5, 'Pantera', 'Zahvatuivavshii', 'Ciornie Spitzi Ciornii Lesi');