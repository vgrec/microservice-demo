package org.microservices.dogs.repo;

import org.microservices.dogs.entity.Dog;
import org.springframework.data.repository.CrudRepository;

public interface DogRepository extends CrudRepository<Dog, Long> {
}
